##Markdown?!     
###4 september 2017
De eerste dag van de design challenge waar we te horen krijgen dat we deze blog moeten bijhouden. We kregen aan het begin van de dag uitleg over de design challenge en over de andere deliverables. Op deze dag stond vooral het onderzoeken centraal. Na veel brainstormen zijn we uiteindelijk individueel begonnen met ons onderzoek visueel te maken. We kwamen ook al met een paar ideeën voor ons spel wat we later verder uit gaan werken.

###5 september 2017 
Het eerste hoorcollege van het jaar is vandaag van start gegaan. Er is uitgelegd wat design in het kort inhoud en hoe het ontwerpproces te werk gaat.
         
###6 september 2017
Vandaag kwamen we bijeen om verder te gaan met het visueel maken van een thema en doelgroep. Dit werd individueel gedaan. Mijn thema was sport en mijn doelgroep is alle eerstejaars cmd studenten. Op een gegeven moment liep ik persoonlijk een beetje vast en heb daarom om feedback gevraagd bij de docent. Ik heb bij een aantal medestudenten navraag gedaan naar wat voor soort sport zij beoefenen. Deze informatie heb ik vervolgens verwerkt door middel van een moodboard. Later op de dag heeft de studiecoach wat dingen verteld over teamafspraken en dergelijke. Ook hebben we nog uitleg gekregen over het maken van een blog.

###7 september
Vandaag hebben we het eerste werkcollege gehad. Dit werkcollege is aansluitend op de theorie van het hoorcollege van dinsdag. We moesten van een huidige situatie een gewenste situatie maken. Dat moesten we vervolgens tekenen en presenteren.

###11 september 2017
Vandaag kwamen we bijeen om een begin te maken aan een paperprototype. We begonnen eerst met het nadenken over een definitief concept van onze game. Nadat we eindelijk een goed concept hadden bedacht gingen we verder denken over wat er nog allemaal bij komt kijken. Zo kwamen met een goed online element in de vorm van een website en met een klein bordspel als offline element. Dit bordspel is geschetst en klaar voor het eerste gebruik. Verder zijn er vragen verzonnen die later op een kleiner formaat kaart worden gemaakt.

###12 september 2017
Het tweede hoorcollege ging over verbeelden. We hebben het over de wetten van gestalt, semiotiek en retorica gehad.

###13 september 2017
Vandaag kwamen we weer bijeen om ons paperprototype goed te maken om te kunnen presenteren. We hebben de gemaakte vragen en opdrachten uitgeprint en uitgeknipt. De spelregels zijn nog verder uitgewerkt en voor de presentatie nog even opgeschreven. Tijdens het presenteren werden er vragen gesteld en later hebben de teams nog een tip en een top opgeschreven. Deze tips gaan we nu verder verwerken om ons paperprototype nog net iets beter te maken voor de deadline.

###14 september 2017
Vandaag was het weer tijd voor een werkcollege, maar voor het werkcollege hebben we ons spel gespeeld. We hebben een spelanalyse gedaan en kwamen een paar fouten tegen die we meenemen voor de volgende iteratie. Bij het werkcollege moesten we verschillende woorden verbeelden binnen 15 seconde. Dit moest eerst met fineliner en daarna met een marker. Vervolgens moesten we van de plaatjes die we meegenomen hadden bepalen of het pathos, logos of ethos was.